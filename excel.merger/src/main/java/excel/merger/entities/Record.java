package excel.merger.entities;

import java.util.HashMap;
import java.util.Map;


public class Record {
	private String primaryKey;
	
	private Map<Integer, Object> attributes;

	public Record(String primaryKey) {
		super();
		this.primaryKey = primaryKey;
		this.attributes = new HashMap<Integer, Object>(30);
	}

	public Record() {
		super();
		this.attributes = new HashMap<Integer, Object>(30);
	}

	public void addAttribute(Integer key, Object value){
		attributes.put(key, value);
	}
	
	public void deleteAttribute(Integer key){
		attributes.remove(key);
	}
	
	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Map<Integer, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<Integer, Object> attributes) {
		this.attributes = attributes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((primaryKey == null) ? 0 : primaryKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (primaryKey == null) {
			if (other.primaryKey != null)
				return false;
		} else if (!primaryKey.equals(other.primaryKey))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Record [primaryKey=" + primaryKey + ", attributes="
				+ attributes + "]";
	}
	
	
}
