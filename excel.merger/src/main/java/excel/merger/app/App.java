package excel.merger.app;

import java.util.List;

import excel.merger.entities.Record;
import excel.merger.merger.XLSXMerger;
import excel.merger.readers.XLSXReader;
import excel.merger.writer.XLSXWriter;

public class App {

	public static void main(String[] args) {
		String fileTo = "C:\\Users\\Volha\\Documents\\1.xlsx";
		String fileFrom = "C:\\Users\\Volha\\Documents\\from.xlsx";
		XLSXReader readerTo = new XLSXReader();
		List<Record> recordsTo = readerTo.read(fileTo, 3);
		XLSXReader readerFrom = new XLSXReader();
		List<Record> recordsFrom = readerTo.read(fileFrom, 0);
		int[] gg = new int[] {2,3,4,5,6,7,8,9,10};
		XLSXMerger merger = new XLSXMerger();
		List<Record> merged = merger.merge(recordsFrom, recordsTo, gg);
		XLSXWriter writer = new XLSXWriter();
		writer.write(merged);
	}

}
