package excel.merger.merger;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import excel.merger.entities.Record;

public class XLSXMerger implements IMerger {
	private final static Logger logger = Logger.getLogger(XLSXMerger.class);
	
	@Override
	public List<Record> merge(List<Record> from, List<Record> to,
			int[] mergedIndexesFrom) {
		logger.info("Merging files started");
		List<Record> mergedRecords = new ArrayList<Record>(to);
		for (Record recordFrom: from) {
			findRecordForMerge(recordFrom, mergedRecords, mergedIndexesFrom);
		}
		logger.info("Merging files finished");
		return mergedRecords;
	}

	private void findRecordForMerge(Record from, List<Record> to, int[] mergedIndexes) {
		for (Record merged: to) {
			if (merged.getPrimaryKey().equals(from.getPrimaryKey())){
				int size = merged.getAttributes().size();
				for (int index: mergedIndexes) {
					merged.getAttributes().put(size, from.getAttributes().get(index));
					size++;
				}
				return;
			}
		}
	}
}
