package excel.merger.merger;

import java.util.List;

import excel.merger.entities.Record;

public interface IMerger {
	List<Record> merge(List<Record> from, List<Record> to, int[] mergedIndexes);
}
