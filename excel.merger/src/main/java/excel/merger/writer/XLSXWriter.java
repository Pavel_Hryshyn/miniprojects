package excel.merger.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import excel.merger.entities.Record;

public class XLSXWriter implements IWriter {
	private final static Logger logger = Logger.getLogger(XLSXWriter.class);
	
	@Override
	public void write(List<Record> records) {
		 //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("results");
		
        int rownum = 0;
        
        for (Record record: records) {
        	
        	int size = record.getAttributes().size();
        	Row row = sheet.createRow(rownum++);
        	
        	for (int cellnum = 0; cellnum < size; cellnum++){
        		Object value = record.getAttributes().get(cellnum);
        		Cell cell = row.createCell(cellnum);
        		cell.setCellValue(value.toString());
        	}
        }
        try {
        	//Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("yammer_results.xlsx"));
			workbook.write(out);
			out.close();
		} catch (IOException e) {
			logger.error("IOException occured in the writing time", e);
		}
        logger.info("File with name wrote");
	}

}
