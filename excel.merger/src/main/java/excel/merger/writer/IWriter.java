package excel.merger.writer;

import java.util.List;

import excel.merger.entities.Record;

public interface IWriter {
	void write(List<Record> records);
}
