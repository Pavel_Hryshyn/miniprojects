package excel.merger.readers;

import java.util.List;

import excel.merger.entities.Record;

public interface IReader {
	List<Record> read(String file, Integer pkPosition);
}
