package excel.merger.readers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;









import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.microsoft.schemas.office.visio.x2012.main.CellType;

import excel.merger.entities.Record;

public class XLSXReader implements IReader{
	private final static Logger logger = Logger.getLogger(XLSXReader.class);

	@Override
	public List<Record> read(String file, Integer pkPosition) {
		List<Record> records = new ArrayList<Record>();
		
		try {
			FileInputStream in = new FileInputStream(new File(file));
			
			//hold reference to .xsls file
			XSSFWorkbook workbook = new XSSFWorkbook(in);
			 //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
 
            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            
            while (rowIterator.hasNext()) 
            {
            	int position = 0;
            	Record record = new Record();
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                for (int cellNum = 0; cellNum < row.getLastCellNum(); cellNum++)
                {
                    Cell cell = row.getCell(cellNum, Row.CREATE_NULL_AS_BLANK);
                    
                    Object recordAttribute = null;
                    //Check the cell type and format accordingly
                    switch (cell.getCellType()) 
                    {
                        case Cell.CELL_TYPE_NUMERIC:
                        	recordAttribute = cell.getNumericCellValue();
                            break;
                        case Cell.CELL_TYPE_STRING:
                        	recordAttribute = cell.getStringCellValue();
                            break;
                        case Cell.CELL_TYPE_FORMULA:
                        	recordAttribute = cell.getCellFormula();
                            break;
                        case Cell.CELL_TYPE_BLANK:
                        	recordAttribute = "";
                            break;
                        default:
                        	recordAttribute = "";
                        	break;
                    }
                    record.addAttribute(position, recordAttribute);
                    if (position == pkPosition){
                    	record.setPrimaryKey(cell.getStringCellValue());
                    }
                    position++;
                }
                records.add(record);
            }
            workbook.close();
            in.close();
            logger.info("File with name " + file + " read and closed");
		} catch (FileNotFoundException e) {
			logger.error("File doesn't found", e);
		} catch (IOException e) {
			logger.error("Cannot create workbook",e);
		}
		return records;
	}
}
